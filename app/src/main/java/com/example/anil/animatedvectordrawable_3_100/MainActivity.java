package com.example.anil.animatedvectordrawable_3_100;

//setting programmatically with 3 files

import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.AnimationDrawable;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageView  = findViewById(R.id.image);

        imageView.setBackgroundResource(R.drawable.triangle_animated_vector);


        final AnimatedVectorDrawable animatedVectorDrawable = (AnimatedVectorDrawable) imageView.getBackground();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animatedVectorDrawable.start();
            }
        });

        /*AnimatedVectorDrawableCompat animatedVectorDrawableCompat = AnimatedVectorDrawableCompat.create(this, R.drawable.triangle_animated_vector);
        imageView.setImageDrawable(animatedVectorDrawableCompat);

        animatedVectorDrawableCompat.start();*/
    }
}
